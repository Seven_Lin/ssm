# Day 02

## 知识回顾  

### MAVEN项目管理

1）jar包，大型项目调用很多第三方的jar，只需要写pom.xml，它就根据写的某个依赖，它自动下载这个jar包，如果这个jar（class）如它又调用其他第三方jar，也会随之下载。  
2）怎么*管理这些jar，它通过三级仓库  
中央仓库，全球唯一，maven官网，Apache组织  
镜像仓库，（私服）:git镜像仓库，码云，每10分钟去更新一次。然而它开始时都是空  
本地仓库，本机，每位同学都有一个本地仓库  
这样的设计随时都可以把本地仓库清空。（会有这种情况的:新机器，jar访问出错，用别人的覆盖）  
3）所有项目用的jar都放在一个本地仓库中，他们不会冲突吗？  
发明:坐标，多级目录，通过目录起到分类作用  
groupId 组ID，域名倒写  
artifactId 构建Id，项目名称  
version 版本号  
各称厂商都会发布到Maven中央仓库，不允许，全球唯一！  
4）maven命令，它会自动执行前面阶段命令，eclipse/idea  

### SpringBoot和Maven有什么关系？

它们目标根本就不同  
Maven项目构建工具，SpringBoot本质把开发者引入到Spring Cloud微服务器路上  
Spring Boot基于Maven，它也可以项目构建工具,SSM三大框架  
新的好处:  
1）Maven需要开发者自己去维护pom.xml，特别添加可以来，排除依赖冲突  
2）它Spring团队它实现父项目，把主流jar都加好了，我们无需自己去写，它集成好了  
3）如果项目完成好，就和Maven无关，SpringBoot全面接管项目启动和加载  

著名的错误:  
404错误，网页链接没有找到，springmvc它的controller就没有找到  

### Tomcat web中间件

nodejs运行JavaScript容器，tomcat运行Java的容器(Servlet)  
glassesfish、jboss、webspare、weblogic（牛，收费）  
现在的项目都是分布式，集群，很多机器（几十台，几百台甚至更多）  
微服务架构对资源（硬件、软件环境（中间件））减低  

写反射代码，反射是所有框架的底层，SSM，不影响后续的开发，能听懂，能掌握就更好了，有利大家对底层了解   

浏览器: http://localhost:8080/hello ，怎么找到Hello Controller.hello方法并且调用  
```java

Hello Controller hc = new HelloController();//创建对象  
hc.hello();//调用它的hello方法

```

在springmvc（spring）框架IoC控制反转，它来管理对象  
1）Spring它底层去创建对象  
2）它底层去调用对象  

3）怎么把 http://localhost:8080/hello 和hello方法对应呢？  
注解: ```@ReqestMapping(“/hello”)``` 规定全局唯一，它是在某个方法之上  

## SpringMVC

### 怎么使用SpringMVC

为什么需要SpringMVC，先了三大框架各自负责内容:  
SpringMVC 要完成框架任务里面有很多组件:  
1）DispatcherSevlet底层Java web规范servlet。封装  
2）HandlerMapping 处理器映射，url解析  
3）HandlerAdapator 处理器适配器，找到Controller  
4）Car Controller实现请求映射，调用它业务方法，最终响应reponse  
5）返回ModelAndView被Model（car），返回Java对象  

![ssm三大框架流程](https://gitee.com/Seven_Lin/picbed/raw/master/img/20210222114203.png)

### 案例:汽车信息从系统中获取

请求:http://localhost:8080/car/get  
结果:页面上展示car里的数据json字符串  
car.java对象，利用@RestController把Java对象转换json字符串返回  

思路: 请求这个链接，返回json字符串，返回ajax请求，把json字符串转换js对象  
js对象利用Vue解析里面数据，最终在页面展现

保时捷718 Cayman T,红色，641000元起  

### 开发步骤: 

1）创建Springboot项目（maven）
2）carController.java  
3）Car.java  

### 404错误，挨个地方检查，通用错误  

1）Controller类上面注解@RestController  
2）注意路径  
3）url浏览器输入:http://localhost:8080/car/get 与@RequestMapping("/car/get")对应  
4）刷新项目，有环境缓存，project/clean清除下项目缓存（bug）  
5）把老师项目创建，src下的内容覆盖+pom.xml检查，删除target项目  

### 案例:学生信息结合，html表单和后台代码结合 

找到web/day03  
表单中填写数据，提交到后台（SpringMVC）  
重点学习，SpringMVC如何接收到这些参数  

表单本身就可以提交信息到后台  
```html
<form action="url" method="get/post"></form>
```
标签，所有要提交的表单空间必须放在这个标签中，放在外面是不会提交  
习惯在body标签下就写form  
还要加一个提交按钮，submit，他点击完成会自动提交action指定的地址  
http://localhost:8090/stu/add  
?name=lin  若是name没有为空字符串，null  
&age=18  
&sex=1  
&hobby=%E4%B9%92%E4%B9%93%E7%90%83  （Unicode编码，汉字按u8进行编码）  
&edu=1  
&submit=%E6%8F%90%E4%BA%A4  

get请求方式的特点，html规范自动把表单中的组件所有值拼接成一个字符串  
key就是组件name（早期）或者id（新）就，value就是你填写的值  
如果组件名称相同name，id，后台会以一个数组来接受  

get和post请求差异  
get是直接可以在浏览器url中看到参数，第一个参数？，后面的多个参数&  
而post请求把这些参数都隐藏，你看不见！！会隐藏浏览器请求中，传递后台  
http://localhost:8090/stu/add 

SpringMVC负责是把请求中数据传递Controller（接受这些参数）  
1）接受单个参数,接受id，html隐藏框 input type=”hidden“ 不在页面展示，但是会传递到后台
```html
  <!-- 习惯把隐藏框写在form下面，这个不是给用户看的，给开发者（修改） -->
        <input type="hidden" name="pid" id="pid" value="666">
```
http://localhost:8090/stu/add?pid=666&name=&age=&sex=1&edu=1&submit=%E6%8F%90%E4%BA%A4  
pid=666 为隐藏框  
2）接受多个参数，接受name，age，hobby数组  
3）接受对象 Student (Model)  
4）url特殊RESTFul形式  

开发步骤:
1) StudentController  
   a.接受单个参数 pid  
   b.接受多个参数 name，age，hobby  
2） Student.java POJO(私有属性，利用接受参数)  
3） RESTFUL(接受少量参数)  
   
页面错误  
404  
400 类型转换错误  
@RequestParam("id") 注解要求严格，页面必须有id参数提交，没有就报类型转换错误  
日期:英文日期:2021/2/22  中文日期 :2021-2-22  
解决: 
```java

    @DateTimeFormat(pattern = "yyyy-mm-dd") //这个注解声明日期格式(yyyy-mm-dd)
    private Date eduTime;

```





