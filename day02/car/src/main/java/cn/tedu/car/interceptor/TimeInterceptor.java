package cn.tedu.car.interceptor;

import org.springframework.stereotype.Component;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@Component
//拦截所有方法，打印执行时间
public class TimeInterceptor implements HandlerInterceptor {
//    现在这样写法会引起并发问题
//    定义成员变量两个方法就可以共享这个变量
//    在类的不同方法中共享，而且避免线程问题
    ThreadLocal<Long> startTimeTl = new ThreadLocal<Long>();
//    private long startTime;
    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
//    startTime 获取当前时间 毫秒值 返回long类型
//        startTime=System.currentTimeMillis();
        Long startTime=System.currentTimeMillis();
        startTimeTl.set(startTime);
        return true;//放行
    }

    @Override
    public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler, ModelAndView modelAndView) throws Exception {
       long  endTime = System.currentTimeMillis();
       long startTime = startTimeTl.get();
       startTimeTl.remove(); //必须写 不写会发生内存泄漏
//        加上小括号解决优先级问题
        System.out.println("执行耗时:"+(endTime-startTime)+"ms");
    }
}
