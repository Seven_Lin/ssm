package cn.tedu.car;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

//它必须放在controller类的父目录中，如果目录不对Controller不会加载（包扫描）

@SpringBootApplication //标识这是一个Springboot程序
public class RunApp {
    public static void main(String[] args) {
        SpringApplication.run(RunApp.class,args);
    }
}
