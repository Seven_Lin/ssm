package cn.tedu.car.controller;

import cn.tedu.car.service.CarService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import cn.tedu.car.pojo.Car;

//保时捷718 Cayman T,红色，641000元起
//@Controller //SpringMVC底层会创建这个对象实例,返回的是Java对象
//@ResponseBody //Controller+ResonseBody = RestController
@RestController //返回的Java对象再次进行转换json字符串

public class CarController {
    @Autowired//carService对象直接再beans容器中，实现set注入
//    接口时对外开放的，也是规范的，所以连接接口
    private CarService carService;//体现对象的关联关系
    //    网页:http://localhost:8080/car/get
    @RequestMapping("/car/get")
    public Car get() {
        System.out.println("car get");
        return carService.get();
    }
}


//@Service //Spring框架
//public class CarService{}