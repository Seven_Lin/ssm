package cn.tedu.car.pojo;

//model模型，view视图，controller控制:MVC控制
//保时捷718 Cayman T,红色，641000元起
//POJO 它使用各个属性使用包装类型，和后期相比，简单的Java对象
public class Car {
//设置属性
    private Integer id;//    编号 718
    private String name;//保时捷
    private String type;//类型 Cayman T
    private String color; // 颜色
    private Double price; //641000元

//    一堆get xxx: 获取/setXXX :设置


    public Integer getId() {//获取id方法
        return id;
    }

    public void setId(Integer id) {//设置id方法
        this.id = id; //前面id类的属性，后面id的参数
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

//    这个是为了调式方便，看对象中属性的值，不是POJO需要的

    @Override
    public String toString() {
        return "car{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", type='" + type + '\'' +
                ", color='" + color + '\'' +
                ", price=" + price +
                '}';
    }
}
