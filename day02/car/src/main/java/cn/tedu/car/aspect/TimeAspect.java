package cn.tedu.car.aspect;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

@Component //让spring进行创建实例
@Aspect //切面定义，默认没有这个包，需要导包
//@Order(1)//通过这个标识符排序
public class TimeAspect {
    //空方法，写切点表达式
//    execution(public * 包路径..（多级，）*(..（多参数）))
    @Pointcut("execution(public * cn.tedu.car.service..*(..))")
    private  void aopPointCut(){

    }
//    方法，配置环绕通知,参数，joinPoint连接点（拦截对象）
    @Around("aopPointCut()")
    public Object around(ProceedingJoinPoint joinPoint) throws Throwable {
        long startTime = System.currentTimeMillis();
//        调用业务来回调 类似invoke
//        不调用获取对象等同于不处理 不适用AOP
        Object rtn = joinPoint.proceed();
        long endTime = System.currentTimeMillis();
        String name = joinPoint.getTarget().getClass().getName()//被拦截的类名名称
                +"#"+joinPoint.getSignature().getName();//被拦截的方法名称
        System.out.println(name+"执行耗时:"+(endTime-startTime));
        return rtn;
    }
}
