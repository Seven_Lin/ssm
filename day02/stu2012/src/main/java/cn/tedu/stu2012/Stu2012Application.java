package cn.tedu.stu2012;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Stu2012Application {

    public static void main(String[] args) {
        SpringApplication.run(Stu2012Application.class, args);
    }

}
