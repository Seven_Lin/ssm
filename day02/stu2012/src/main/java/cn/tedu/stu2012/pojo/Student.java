package cn.tedu.stu2012.pojo;

import org.springframework.format.annotation.DateTimeFormat;

import java.util.Arrays;
import java.util.Date;


public class Student {
    private Integer pid;
    private String name;
    private Integer age;
    private String sex;
    private String[] hobby;
    private String edu;
    @DateTimeFormat(pattern = "yyyy-mm-dd") //这个注解声明日期格式(yyyy-mm-dd)
    private Date eduTime;

//    getter和setter
    public Date getEduTime() {
        return eduTime;
    }

    public void setEduTime(Date eduTime) {
        this.eduTime = eduTime;
    }

    public Integer getPid() {
        return pid;
    }

    public void setPid(Integer pid) {
        this.pid = pid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public String[] getHobby() {
        return hobby;
    }

    public void setHobby(String[] hobby) {
        this.hobby = hobby;
    }

    public String getEdu() {
        return edu;
    }

    public void setEdu(String edu) {
        this.edu = edu;
    }

//    toString 是为了方便查看和调用


    @Override
    public String toString() {
        return "Student{" +
                "pid=" + pid +
                ", name='" + name + '\'' +
                ", age=" + age +
                ", sex='" + sex + '\'' +
                ", hobby=" + Arrays.toString(hobby) +
                ", edu='" + edu + '\'' +
                ", eduTime=" + eduTime +
                '}';
    }
}
