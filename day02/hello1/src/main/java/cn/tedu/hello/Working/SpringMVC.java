package cn.tedu.hello.Working;

import org.springframework.web.bind.annotation.RequestMapping;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;//反射包
import java.util.Arrays;

/*
 * 模拟Spring+SpringMVC底层实现
 * 框架底层利用反射技术:下面反射都是死语法api，这些只能死记
 * 1）获取类
 * 2）创建对象实例
 * 3）获取方法
 * 4）获取方法上的注解，获取注解的value
 * 5）回调（callback）invoke方法，不是直接调用
 * */
public class SpringMVC {
    public static void main(String[] args) throws ClassNotFoundException, IllegalAccessException, InstantiationException, NoSuchMethodException, InvocationTargetException {
    String url ="/hello";    //http://localhost:8080/hello  为了给注解上匹配
        //        全局限定名=包路径.类名
//        黄色的提示，可以不改 ，提醒加泛型<?>
        Class clazz = Class.forName("cn.tedu.hello.Working.Controller.HelloController");//获取HelloController这个类
//        2、创建对象实例 Object 是因为不知道获取的对象是什么
        Object instance = clazz.newInstance();//新建实例instance
        System.out.println(instance);
//        3、获取指定方法
        Method m = clazz.getMethod("hello");//获取这个类的hello方法
//        5、通过方法或者上面注解，通过注解获取value值，等价url=/hello
//        已知，业务
        RequestMapping an = m.getAnnotation(RequestMapping.class); //获取方法上的指定注解
//        最终底层就可以根据url=/hello 和这里进行匹配，就执行这个注解所属这个方法（唯一）
        System.out.println(Arrays.toString(an.value()));//["/","/hello"]
        if (an != null){
            //       4、方法回调,执行hello方法，参数是对象实例
            String trn = (String)m.invoke(instance);//返回Object，强制转换字符串
            System.out.println(trn);
        }
        System.out.println("执行完成");
    }

}
