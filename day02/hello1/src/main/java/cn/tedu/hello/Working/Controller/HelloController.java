package cn.tedu.hello.Working.Controller;

import org.springframework.web.bind.annotation.RequestMapping;

public class HelloController {
//    浏览器:http://localhost8080/hello ,传入:/hello
    @RequestMapping({"/","/hello"})
    public String hello(){
        return "springmvc 底层实现";
    }
}
