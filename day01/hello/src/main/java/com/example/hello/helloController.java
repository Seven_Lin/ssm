package com.example.hello;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController //标识这个类是一个Contrler
//Rest标识返回结果，Java对象转换json字符串，在页面展示json字符串内容

public class helloController {
//页面请求：http://localhost:8080/,http://localhost:8080/hello
    @RequestMapping({"/","/Hello"})
    public String hello(){
        //    添加方法，返回一个字符串
        return "hello springmvc";
    }
}
