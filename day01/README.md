# day01

## 知识回顾

### vue+element-ui前端界面

element-ui 很多大公司采用，写法，语法  
vue数据驱动，组件化Item.vue  
1）数据驱动，体系开发者关心不在是代码api和数据（业务），只关心数据（业务）  
2）组件化，复用，一旦创建好一个组件，这个组件就可以给其他人共享

### 自己定义一个组件Item.vue

1）.vue文件，它有3部分组成  
a.template模板，html+vue片段（spa单页面开发，它只是这个页面一部分）  
b.script脚本，形式export default{},本质js脚本，数据区，方法区  
c.style样式，只对自己组件有效，避免不同冲突  
2）注册，vue项目（工程），所有组件都是App.vue根组件的子组件  
Import导入，注意路径，相对路径./ 当前目录 、../ 上级目录  
3）使用```<item></item>```标签

### 现金开发拿来注意，站在巨人肩膀上开发，好处开发效率高

组件:element-ui完美，它形成一套完整的ui界面，比html原生美观很多

### 列表

```<table></table><tr></tr><td></td>``  
至少写两行，表格头，表格行内容，单元格  
```<el-table><el-table-column>```  
只写一行，代码量少，既有label表格头的信息，又有prop单元格信息  
表格内容是活的，设定一个数据源（js对象，json，数据库结果集）  
对list的js对象获取其属性解析过程，是由vue+element-ui底层实现  
JSON.parse(jsonstr)变成js对象

ajax请求后台Java（springmvc）把后台从数据库返回结果集封装到Java对象中  
把Java对象转换json字符串（jackson工具包api），返回给ajax  
Ajax底层把接送字符串转换js对象 success方法返回js对象

拿到js对象，对象数组  
foreache(o in array)  
v-for(o,i in cars)

### 新增功能

1）点击新增按钮，执行toadd事件，清空this.m数据，打开对话框，设置新增表示tag

``` vue.js

toadd: function() {
      // 防止数据已经有值，干扰，清空数据
      this.m = {};
      // 展现对话框,要调用数据区域的数据必须加this关键字
      this.dialogVisible = true;
      this.isUpdate = false;
    }

```

2）点击修改按钮，底层通过修改按钮获取它所在的td，进一步它父元素tr，获取到整个tr就可以遍历里面每个td  
获取到每个td内容，拼成row对象{title:"a",sellpoint:"b"}  
```JSON.parse(JSON.stringify(row));```经过两次转换，形成new Object（），新对象，不是一个引用  
3）保存按钮，复用，新增和修改都是一个方法，习惯使用的方法  
this.list.splice(索引位置，删除条数，新增js对象);  
新增: 0 0 m;  
删除:index 1;  
修改:index,1,m;  
4）如何拿索引值？  
v-for="row，index in list";

## 后端学习

![框架图](https://gitee.com/Seven_Lin/picbed/raw/master/img/20210220103058.png)

![流程图](https://gitee.com/Seven_Lin/picbed/raw/master/img/20210220103138.png)

### maven

项目管理工具  
1）管理jar包  
传统项目管理jar方法:  
a.复制jar（老师给qr.jar，单位老程序员）  
b.创建Java工程，创建lib目录，把jar放入其中  
c.build path 项目中引用这个jar d.Java代码中就可以使用

Maven它来管理jar（中央仓库，镜像仓库，本地仓库）  
Maven解决依赖版本冲突问题（半，你去解决部分问题，阿里dubbo，自己处理）  
SpringBoot在Maven基础上（全面解决版本冲突，99%由sping组织解决）  
习惯：ssm xml方式，注解方式,springboot
2）依赖  
  坐标：
3）命令  
  mvn 各阶段操作  

  ### 管理jar包  

  体现：仓库、依赖、坐标  
  1）从哪里找到jar?远程仓库、镜像仓库、本地仓库  
  2）把它放在那里？  
  3）怎么调用？  

  下载maven.jar  
  官网：http://maven.apache.org/download.cgi  
  下载文件：apache-maven-3.6.3-bin.tar.gz  

  安装配置settings.xml  
  ${user.home}/.m2/repository 存储位置不建议使用c盘  


  默认仓库：中央仓库（国外网站）  
  镜像仓库：默认关闭，自行配置，阿里镜像仓库  

  https://start.spring.io/ spring initializer的提示  

  ```java
package com.example.demo.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController //标识这个类是一个Contrler
//Rest标识返回结果，Java对象转换json字符串，在页面展示json字符串内容

public class HelloController {
    //页面请求：http://localhost:8080/,http://localhost:8080/Hello
    @RequestMapping({"/","/Hello"})
    public String hello(){
        //    添加方法，返回一个字符串
        return "hello springmvc";
    }
}
```
```java
package com.example.demo;

import com.sun.deploy.si.SingleInstanceImpl;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication//标识这是一个Springboot程序
public class DemoApplication {


    public static void main(String[] args) {
        //        第一个参数是类名字
                SpringApplication.run(DemoApplication.class,args);
    }

}

```



