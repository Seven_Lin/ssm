package com.example.demo;

import com.sun.deploy.si.SingleInstanceImpl;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication//标识这是一个Springboot程序
public class DemoApplication {


    public static void main(String[] args) {
        //        第一个参数是类名字
                SpringApplication.run(DemoApplication.class,args);
    }

}
