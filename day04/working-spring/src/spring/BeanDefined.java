package spring;

public class BeanDefined {
    private String beanName; //bean的名称  类似之前的hello
    private String className; //全局限定名

    public String getBeanName() {
        return beanName;
    }

    public void setBeanName(String beanName) {
        this.beanName = beanName;
    }

    public String getClassName() {
        return className;
    }

    public void setClassName(String className) {
        this.className = className;
    }

    @Override
    public String toString() {
        return "ClassPathXmlApplicationContext{" +
                "beanName='" + beanName + '\'' +
                ", className='" + className + '\'' +
                '}';
    }
}
