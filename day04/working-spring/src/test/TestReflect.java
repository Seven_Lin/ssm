package test;

import cn.tedu.pojo.Dept;
import cn.tedu.pojo.Hello;
import cn.tedu.pojo.User;
import org.junit.Test;
import spring.annotation.Autowired;

import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

public class TestReflect {


    @Test//标识为test
    //    创建对象
    public void createObject() throws Exception{
//    创建hello对象实例？
        String className = "cn.tedu.pojo.Hello";
//        获取类
        Class<?> clazz = Class.forName(className);
//        创建对象
        Hello hello1 = (Hello) clazz.newInstance();
        System.out.println(hello1);
//       调用对象方法
        hello1.hi();
        hello1.welcome();
    }
    @Test
//    执行方法（获取方法）
    public void excuteMethod() throws Exception {
//  利用反射api，获取类的所有方法，得到method对象数组
        String className= "cn.tedu.pojo.Hello";
//        1、获取类名
        Class<?> clazz =  Class.forName(className);
//        4、创建对象
        Object instance = clazz.newInstance();
//      2、获取到当前类的所有方法
//      Method[] ms =clazz.getMethods(); //有很多父类方法干扰
        Method[] ms = clazz.getDeclaredMethods(); //不会展现父类方法
//        3、遍历方法 输出
        for (Method m : ms){
            System.out.println(m.getName());//打印方法名
            //  5、 反射invoke回调来执行某个具体方法
            m.invoke(instance);
      }


    }
    //    注解，特定去执行某件事情
    @Test
    public void annotation() throws Exception {
//     Autowired，加它的方法运行，不加它的方法不运行
        //  利用反射api，获取类的所有方法，得到method对象数组
        String className = "cn.tedu.pojo.Hello";
//        1、获取类名
        Class<?> clazz = Class.forName(className);
//      因为回调函数中的参数为Object类
        Object instance = clazz.newInstance();
//     2、 获取到当前类的所有方法
//      Method[] ms =clazz.getMethods(); //有很多父类方法干扰
        Method[] ms = clazz.getDeclaredMethods(); //不会展现父类方法
//        3、遍历方法 输出
        for (Method m : ms) {
//          获取这个方法上的注解
//            Autowired au = m.getAnnotation(Autowired.class); //获取指定注解
            Annotation an = m.getAnnotation(Autowired.class);
            if (an != null) { //根据是否存在来作为判断一句
//                5、回调
                m.invoke(instance);
            }
        }
    }
        //    给类的属性获取，动态设置值
    @Test
    //DI依赖注入
        public void setFiles () throws Exception {
//        1、获取类
            Class<?> clazz = Class.forName("cn.tedu.pojo.User");
            Object instace = clazz.newInstance();

//            假设从容器中获取Dept对象
            Dept dept = new Dept();

//            2、获取类的所有属性
//            clazz.getFields();  //返回公有public属性
            Field[] fs = clazz.getDeclaredFields();//返回含私有属性
//        3、遍历类获取类中的属性
            for (Field f : fs){
//                私有属性开关打开，安全Java源码没有安全性
                f.setAccessible(true); //才能操作私有属性，否则false不能操作
//                输出属性
                System.out.println(f.getName());
//                获取属性注解，判断是否进行set注入
               Annotation an =  f.getAnnotation(Autowired.class);
               if (an!=null){
//                    反射，set注入 User.setDept(dept);
//                   第一个参数属性所属对象实例，第二个参数要绑定对象
                   f.set(instace,dept); //关联的对象就绑定了
               }
                System.out.println(instace);
            }
        }
    }