package cn.tedu.pojo;

public class School {
    private String schName ="达内";

    public String getSchName() {
        return schName;
    }

    public void setSchName(String schName) {
        this.schName = schName;
    }

//    toString

    @Override
    public String toString() {
        return "School{" +
                "schName='" + schName + '\'' +
                '}';
    }
}
