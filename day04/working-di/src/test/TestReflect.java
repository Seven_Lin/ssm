package test;

import cn.tedu.pojo.Hello;
import cn.tedu.pojo.School;
import org.junit.Test;
import spring.annotation.Autowired;

import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import cn.tedu.pojo.School;

public class TestReflect {



    //    5、标识
    @Test
//    创建对象实例
public void createObject() throws Exception {
//    1、获取类名
    Class clazz = Class.forName("cn.tedu.pojo.Hello");
//    2、创建对象
    Hello hello = (Hello) clazz.newInstance();
//    3、打印hello对象 对象输出的是地址值:cn.tedu.pojo.Hello@22927a81
    System.out.println(hello);
//    4、调用hello对象的方法
    hello.hi();
}

//7、添加标识
    @Test
//    获取方法
//    方法可以调用 .getMethod("指定方法名")  也可以.getMethods() 直接获取所有方法，返回一个数组
//    这里调用.getMethods Hello.java 有两个方法 say(),hi()
public void excuteMethod() throws Exception {
//    1、获取类名
    Class<?> clazz = Class.forName("cn.tedu.pojo.Hello");
//    6、因为invoke要调用对象，所以我们创建对象
    Object instance = clazz.newInstance();
//    2、使用getMethods方法
//    clazz.getMethods();//这个方法输出时会包含父方法
    Method[] ms =clazz.getDeclaredMethods();//这个方法输出时不包含父方法
//    3、遍历数组，输出方法
    for (Method m: ms) {
//      4、  打印方法名
        System.out.println(m.getName());
//        5、回调方法
        m.invoke(instance);
    }


}
@Test
//    使用注解，特定去执行
public void annotation() throws Exception {
//    1、获取类名
    Class<?> clazz = Class.forName("cn.tedu.pojo.Hello");
//    6、因为invoke要调用对象，所以我们创建对象
    Object instance = clazz.newInstance();
//    2、使用getMethods方法
//    clazz.getMethods();//这个方法输出时会包含父方法
    Method[] ms =clazz.getDeclaredMethods();//这个方法输出时不包含父方法
//    3、遍历数组，输出方法
    for (Method m: ms) {
//    获取方法上的注解
////        第一种获取注解的方法
//        Autowired au = m.getAnnotation(Autowired.class);
//        第二种获取注解的方法
        Annotation an = m.getAnnotation(Autowired.class);
//        判断an不等于空时调用方法
        if (an != null) {
//        5、回调方法
            m.invoke(instance);
        }
    }
}
@Test
    //DI依赖注入
    public void setFiles () throws Exception {
//        1、获取类名
        Class clazz = Class.forName("cn.tedu.pojo.Student");
//        因为set需要对象，所以设置一个对象
        Object instance = clazz.newInstance();
//        2、获取属性
//    clazz.getFields() //这个是不包含私有属性
        Field[] fs = clazz.getDeclaredFields(); //这个包含私有属性，但是有开关
//        模拟容器 ，
        School school = new School();
//        遍历数组
        for (Field f: fs) {
//            将私有属性开关打开 输出私有属性
            f.setAccessible(true);
//            打印属性名
            System.out.println(f.getName());

            //        获取属性上的注解
            Annotation an = f.getAnnotation(Autowired.class);
//            判断an是否为空,不为空设置值
            if (an != null){
                f.set(instance,school);
            }
//            打印对象内容
            System.out.println(instance);
        }

    }
}
