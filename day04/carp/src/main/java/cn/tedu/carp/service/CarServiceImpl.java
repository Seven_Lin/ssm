package cn.tedu.carp.service;

import cn.tedu.carp.pojo.Car;
import org.springframework.stereotype.Service;

@Service //标识为服务层
public class CarServiceImpl implements CarService{

    @Override
    public Car get() {
        //        Repository、Database 模拟
        Car c = new Car(); //模型就是临时保存数据，在各层中传递
//        设置car中的值
        c.setId(718);
        c.setName("保时捷");
        c.setType("Cayman T");
        c.setColor("红色");
        c.setPrice(641000.0);
        return c;
    }
}
