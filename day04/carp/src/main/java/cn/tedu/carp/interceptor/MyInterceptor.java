package cn.tedu.carp.interceptor;

import org.springframework.stereotype.Component;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

//写拦截器 ， 并且告诉Spring 创建实例 所以使用@Component
//连接接口
@Component //实现HandelerInterceptor接口
public class MyInterceptor implements HandlerInterceptor {
    //    这里步提示方法，快捷键生成
    @Override //true代表放行，false代表拦截，后面代码都不会执行
//    业务方法执行之前
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        System.out.println("我是拦截器 preHandle");
        return true;//代表放行
    }

    @Override //业务方法执行之后
    public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler, ModelAndView modelAndView) throws Exception {
        System.out.println("我是拦截器 postHandle");
    }

    @Override
    public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex) throws Exception {
        System.out.println("我是拦截器 completion");
    }
}