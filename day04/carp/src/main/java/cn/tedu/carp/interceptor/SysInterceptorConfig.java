package cn.tedu.carp.interceptor;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Configuration //告诉spring框架我是一个配置类
public class SysInterceptorConfig implements WebMvcConfigurer {
    @Autowired
    private MyInterceptor myInterceptor;

    @Override
    public void addInterceptors(InterceptorRegistry registry) {
//        注册自己写的拦截器,如果时多个拦截器，形成一个拦截器链，
//        拦截器链执行顺序:就是我们注册的顺序,同时也决定了 前面先进入后出
//        顺序:myInterceptor 进> timeInterceptor 进 >timeInterceptor 出 >   myInterceptor 出
        registry.addInterceptor(myInterceptor);
    }
}