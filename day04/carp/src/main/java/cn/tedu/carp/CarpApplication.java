package cn.tedu.carp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CarpApplication {

    public static void main(String[] args) {
        SpringApplication.run(CarpApplication.class, args);
    }

}
