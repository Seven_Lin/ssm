package cn.tedu.carp.controller;

import cn.tedu.carp.pojo.Car;
import cn.tedu.carp.service.CarService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class CarController {
    @Autowired
    private CarService carService;
    @RequestMapping("/car/get")  //申请网页访问
     public Car get(){
         System.out.println("car get");
         return  carService.get();
     }
}
