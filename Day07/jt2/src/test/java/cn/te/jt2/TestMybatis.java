package cn.te.jt2;

import cn.te.jt2.mapper.ItemCatMapper;
import cn.te.jt2.mapper.ItemMapper;
import cn.te.jt2.pojo.Item;
import cn.te.jt2.pojo.ItemCat;
import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;


public class TestMybatis {

    private SqlSessionFactory factory;

    @BeforeEach //初始化创建factory对象
    public void  init() throws IOException {
        InputStream is = Resources.getResourceAsStream("sqlMapConfig.xml");
        factory= new SqlSessionFactoryBuilder().build(is);
    }

    @Test
    public void findItemCat(){
        SqlSession session = factory.openSession();
        ItemCatMapper mapper = session.getMapper(ItemCatMapper.class);
        List<ItemCat> itemCatList =  mapper.findItemCat();
        for (ItemCat ic: itemCatList) {
            System.out.println(ic);
        }
    }
    @Test
    public void findItem(){
        SqlSession session = factory.openSession();
        ItemMapper itemMapper = session.getMapper(ItemMapper.class);
        List<Item> item =  itemMapper.findItem();
        for (Item it: item) {
            System.out.println(it);
        }
    }


}

