package cn.te.jt2.mapper;

import cn.te.jt2.pojo.Item;

import java.util.List;

//商品的接口
public interface ItemMapper {
    public List<Item> findItem();
}
