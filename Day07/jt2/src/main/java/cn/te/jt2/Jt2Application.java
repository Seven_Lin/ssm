package cn.te.jt2;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Jt2Application {

    public static void main(String[] args) {
        SpringApplication.run(Jt2Application.class, args);
    }

}
