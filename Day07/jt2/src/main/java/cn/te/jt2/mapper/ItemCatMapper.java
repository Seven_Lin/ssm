package cn.te.jt2.mapper;

import cn.te.jt2.pojo.ItemCat;

import java.util.List;

public interface ItemCatMapper  {
//      <select id="findItemCat" resultMap="ItemCatItemRM">
//     <resultMap id="ItemCatItemRM" type="ItemCat">
    public List<ItemCat> findItemCat();
}
