# day07

## 知识回顾

### mybatis持久层框架，完成数据库的封装，半面想对象框架  
一边关注时xml中如何配置sql（面向过程编程），一边关注时java模型对象（面向对象编程）  
半ORM映射框架  

### mybatis完成数据库表的结果集和Java模型对象映射  

按表映射：
```sql
select sell_point as sellPoint , t.* from tb_item t
```  
这个语句可知，最终mybatis时从结果集中获取数据，写入对应java对象中  

1）根据jdbc提供元数据:getMata().getColumnneName()，根据列名获取某行值  
sell_point (123)  
2) 反射，获得pojo的私有属性  
private String sellPoint;  
   反射sellPoint.set()  
   
### mybatis 动态sql  

1）占位符，  
```sql
select *
from tb_item where title like '%123%';
```
mybatis中，  
```
select * from tb_item where title like #{title}, 等同于 preparedStatement占位符? 
#{}时预防sql注入的
${}不能预防sql注入的
```
2）parameterType全都支持，  
单值：两种方式:mybatis提供string/int/loing，类似java方式:java.lang.string  
多值：pojo对象，复用，传递数据，参数作用  

3）返回值，  
resultType：string，int，long，pojo，/resultMap，表的字段和pojo名称不同时，多表联查  
4）xml标签   
select查询（find、get、count）、insert新增、update修改、delete 删除  

### mybatis提供3种操作方式 

1）xml方式：必须写映射文件 ItemMapper.xml,session.selectList(),selectOne()  
2）接口方式：面向对象的方式 ItemMapper.find()，它通过cn.tedu.jt.pojo.ItemMapper = xml.namespace(约定)=cn.tedu.jt.pojo.ItemMapper
找到映射文件，find找到某个标签id（唯一），获取sql语句来执行  
本质还是执行xml映射文件！
3）接口+注解方式：把XMl消灭掉（99% ）,多表联查resultMap，需要映射文件  

### 问题所在

查询语句，如果用户没有输入查询条件，此时这个条件忽略的  
删除语句，传递多个参数 in（1，2，3，4）  

动态sql  
标签：if判断/where/set/foreach形成in子查询，id就可以批量删除  
 
两个小的技巧  
1）标签：sql + include  
sql标签定义字符串变量  
include标签引用  

2）pojo去掉包路径，参数，返回值，直接对象名称 cn.tedu.jt.pojo.Item简写Item  
sqlMapConfig配置全局别名  
所有映射文件中的包路径就可以干掉  
只管理映射文件中配置，而不是java对象  

### ResultMap

1）如果数据库表字段（结果集）它和POJO属性名不一致，通过它来做映射  
2）多表查询（查询多个表，在内存种构建一个大大结果集）  
表关联：4种，一对一，一对多，多对一，多对多（两个一对多）  
mybatis，resultMap配置：对一 asociaction，对多 collection  
约束：  
1）多表联查配置主键（id）和需要映射的字段result标签（sell_point）,同名price可以省略不写  
2）要求：返回大大结果集中不能有同名字段，如果有多个id，取第一个id，不会报错，（是坑）  

3张表，  
1）一对多，tb_item_cat商品分类表和tb_item商品表，一对多（一个分类下有多个商品）  
2）对一，tb_item商品表和tb_item_Desc商品描述表，一对一，（一个商品有一个商品描述信息）  

### 实现tb_item_cat和 tb_item关系（一对多）

1）两张表的查询 inner join 写好sql，构建大结果集
```sql
--查询某个分类下是商品信息 
SELECT 
c.id,c.name,
t.id,t.title,t.price 
FROM tb_item_cat c INNER JOIN tb_item t ON c.id = t.cid
```
2）创建java工程  
3）创建ItemCat.java POJO对象   
4）在映射文件ItemMapper.xml中添加ResultMapper映射  
5）测试