package cn.tedu.jt7;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Jt7Application {

    public static void main(String[] args) {
        SpringApplication.run(Jt7Application.class, args);
    }

}
