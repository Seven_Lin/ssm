package cn.tedu.jt7.mapper;

import cn.tedu.jt7.pojo.Item;

import java.util.List;
import java.util.Map;

//接口规定方法
public interface ItemMapper {
//    泛型，List集合中必须放Item对象，其他的不行，约束检查
//       <select id="find" resultMap="itemRM">
//        SELECT * from tb_item
//    </select>

    //    方法名 为 select的id
//    public List<Item> find();//无参的
    public List<Item> find(Item item);
    public Item get(Long id);
    public Integer count();
    //    public void insert();
    public void insert(Item item);
    public void update(Item item);
    public void delete(Long id);
    //    删除多条，使用map集合作为参数，它是一个值，只put一次
    public void deleteMuch(Map<String,Object> map);
}
