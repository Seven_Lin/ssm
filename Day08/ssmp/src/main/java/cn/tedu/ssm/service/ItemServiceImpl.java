package cn.tedu.ssm.service;

import cn.tedu.ssm.mapper.ItemMapper;
import cn.tedu.ssm.pojo.Item;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
@Service//加这个标识后，spring就对它创建实例
public class ItemServiceImpl implements ItemService{
    @Autowired//这个类最终受spring管理，使用这个注解，这个从容器中获取set注入
    private ItemMapper itemMapper;
    @Override
    public List<Item> list(Integer num) {

        return itemMapper.list(num);//调用mybatis最终接口方法（底层给它实现类）
    }
}
