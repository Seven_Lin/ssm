package cn.tedu.ssm.mapper;

import cn.tedu.ssm.pojo.Item;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper //mybatis底层类似spring包扫描，无需使用getMapper();
public interface ItemMapper {
//    这里的的num 是ItemMapper.xml中的${num}
    public List<Item> list(Integer num);
}
