package cn.tedu.ssm.controller;

import cn.tedu.ssm.pojo.Item;
import cn.tedu.ssm.service.ItemService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
@RestController //@Controller 实现标识，springmvc会进行配置，Rest会自动把返回java对象转换json字符串
@CrossOrigin(origins = "*") //声明支持跨域访问，这样8848才能访问8070
public class ItemController {
    @Autowired//自动装配，把service和controller联系起来
    private ItemService itemService;
//    @RequestMapping("/item/list")//和浏览器的url对应  http://localhost:8080/item/list
    @RequestMapping("item/list/{num}/{title}")//RESTFul方式:http://localhost:8080/item/list/{num}
//    public List<Item>list (Integer num){//springmvc自动解析get请求把参数值封装到num参数中
//        return itemService.list(num);
//    }
    public List<Item> list(@PathVariable  Integer num, @PathVariable String title){
        return itemService.list(num);
    }
}
