package cn.tedu.ssm.service;

import cn.tedu.ssm.pojo.Item;

import java.util.List;

//这个和mapper中的ItemMapper是一样的，但是将来业务复杂后，就不一样了
public interface ItemService {
    public List<Item> list(Integer num);
}
