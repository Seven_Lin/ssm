package cn.tedu.pjt;

import cn.tedu.pjt.mapper.ItemMapper;
import cn.tedu.pjt.pojo.Item;
import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;

public class TestSqlConfig {

    private SqlSessionFactory factory;

    @BeforeEach
    public void init() throws IOException {
       InputStream is = Resources.getResourceAsStream("SqlMapperConfig.xml");
       factory = new SqlSessionFactoryBuilder().build(is);
    }
    @Test
    public void findItem(){
        SqlSession session = factory.openSession();
        ItemMapper itemMapper = session.getMapper(ItemMapper.class);
        List<Item> item = itemMapper.findItem();
        for (Item o: item) {
            System.out.println(o);
        }
    }
    @Test
    public void count(){
        SqlSession session = factory.openSession();
        ItemMapper itemMapper = session.getMapper(ItemMapper.class);
        Integer count = itemMapper.findCount();
        System.out.println(count);
    }

}
