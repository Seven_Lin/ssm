package cn.tedu.pjt.mapper;

import cn.tedu.pjt.pojo.Item;

import java.util.List;

public interface ItemMapper {
    public List<Item> findItem();
    public Integer findCount();
}
