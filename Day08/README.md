# day 08

## 知识回顾

jdbc实现和数据库操作，自己拼接sql语句（字符串）  
statment，可能会发生特殊字符，造成sql注入。  
prparedStatement，sql分成两部分  
```sql
select * from tb_item where title like '%诺基亚%'
```
第一个部分: select * from tb_item where title like ?(预编译，速度快)  
第二个部分:'%诺基亚%'替换掉？，防止而已，转码''（安全）  

优点：快捷  
缺点：大量拼接sql字符串，开发者需要专门学习jdbc的api  
Connection、PreaparedStatement、Excecutor。。。  
jdbc原生它无法直接面对大型项目，sql和java代码混杂，后期不易维护，也不是适合大型团队  

随着mybatis框架  
优点：xml中写sql，接口，调用接口都是Java代码，把sql和java代码分开，方便各层开发和应用  
屏蔽jdbc的原生api，开发者无需去了解这些api，就可以只按mybatis开发方式去开发即可  
mybatis自身就支持缓存框架，一级缓存，二级缓存  
jdbc方式每次都要请求数据库连接，耗费数据连接  
mybatis把每次查询会先缓存起来，下一次如果sql一致，不是去数据库查询数据，直接返回缓存中的数据  
缓存都是放在内存中！  

框架都有约定，（约定大于配置）  
分层开发：开发步骤：  
1）全局配置文件sqlMapConfig.xml，配置jdbc事务、datasource数据源（4个参数）、加载有哪些xml映射文件  
（三大框架整合ssm，核心配置大多被spring接管，springboot更加全面接受，配置放在application.yml）  
2）映射文件mybatis核心，它发明了一套支持xml配置方式  
操作标签:select、insert、update、delete  
配合上面标签动态SQL标签:if\where\set\foreach\sql+include  
数据库字段名（数据库的设计规范sql92:全大写，全小写，多个单词使用下划线隔开）和POJO的属性名不一致  
映射:结果集字段和POJO的属性来映射:ResultMap，人工的做一个映射  
3）ResultMap需要两步  
a.定义ResultMap，id标签代表主键，result标签代表普通字段，  
对一:association 声明关联对象  
对多:collection 声明关联对象，搭档，集合表明集合元素类型 ofType  
b.使用ResultMap，给标签的返回值，配置定义ResultMap的id值  
4）提倡面向接口发开，一个接口多个实现类，如果实现类新需求改了，程序升级后，程序报错  
约束行为，接口就约束了它的行为。接口中定义方法，参数，返回值  
mysql驱动，Oracle驱动，底层实现很多不同，java.sql.*  

5）mybatis接口方式发展有两个阶段:  
a.接口假的，它只是利用接口方式调用，最终调用xml映射文件中的配置  
b.接口+注解（mybatis plus）  
底层实现，使用动态代理技术，动态创建实例，类似反射！  
6）测试类调用 session.getMapper...  

### 映射文件中namespace到底有什么用？ 

接口已知，怎么调用xml文件中某个方法呢？select id= find  
1）给定类的文件ItemMapper.class，可获得类的全路径:cn.tedu.jt.mapper.ItemMapper  
2）namespace = cn.tedu.jt.mapper.ItemMapper  
3）获得类的全路径，mybatis加载时，会把所有xml映射文件解析（找到所有xml映射文件，里面namespace获取）  
还有所有标签select，获取这个标签id，（专门技术:domj4j解析文件）  
解析得到:namespace = cn.tedu.jt.mapper.ItemMapper，标签的id，find  
4）最终两个匹配:  
类的全局限定名 = namespace  
标签的id = 执行接口方法  
反射，invoke回调，执行接口方法(mybatis创建实现类:动态)  

### 多表的联查  

一对多而言:一的表称作子表，也称作:主从关系  
需求:一个部门，部门是主，员工是从  
对象关联中:在部门表中:private List<Emp> emps;  

需求:一个员工他所在的部门，员工是主，部门是从  
对象关联关系中:在员工表中:private Dept dept;  

用户只要实现需求1，单项关联    
用户只要实现需求2，单向关联  
用户需要实现需求1和需求2，两句话都需要配置，双向关联  


### ssm整合:

1）后端整合:ssm+springboot构建项目  
2）前端整合:h5+css3+bootstrap+vue+axios（ajax）  

下载几个文件:
1）application.yml
2）res/css/bootstrap.min.css  
3）vue.js和axiso.min.js   

![ssm整合](https://i.loli.net/2021/03/02/XWadHmQGFRzqs17.png)

### 全球最强的连接池  
数据库jdbc，每个请求会创建一个链接connection，链接资源非常有限，100个  
形成连接池，事先先创建多个链接，暂存在池中，你使用时，不是创建新对象，而是从池中获取  
如果结束，关闭对象，还回池中，其它请求就可以复用这个链接  
创建和维护，销毁过程就交给连接池来维护  
hikari日本开发，全球最快链接池，springcloud默认集成连接池  
它遵循jdbc规范，无需写一句代码，只需把driver，换成hikari的配置即可  

### application.properties springboot习惯配置方式  

在三大框架整合，很多mybatis的配置sqlMapConfig.xml中配置就被application.properties文件所代替  
配置的内容就迁移到properties中 
properties文件时有层次的，每个层次前面家两个空格，注意不能以tab键，yml不支持  
yml结构是一个key:value机构，类型map。写时，值前面有一个空格  
记得不能在中间参杂注释，很容易解析错误  

```
配置tomcat端口:8070
server:
  port: 8070
数据库链接配置:5个参数  hikari链接池，数据库厂家驱动，数据库地址，账号 密码
spring:
    datasource:
        type: com.zaxxer.hikari.HikariDataSource
        driver-class-name: com.mysql.cj.jdbc.Driver
        url: jdbc:mysql://localhost:3307/jtdb-small?characterEncoding=utf8&serverTimezone=Asia/Shanghai
        username: root
        password: root
mybatis框架的配置，替代sqlMapConfig.xml
mybatis:
  typeAliasesPackage: cn.tedu.ssm.pojo  配置了POJO的包路径，只写类名即可
  mapperLocations: classpath:mappers/*.xml  指定mappers路径下的 映射文件
                                            resources时maven提出，最终项目不存在的
                                            它在classpath类路径class文件的根目录
                                            java工程编译完成class文件，规则:bin目录下，package
  configuration:
    map-underscore-to-camel-case: true  映射下划线（underscore）到我们驼峰规则（camel-case）
                                         无需resultMap 
    
logging:
  level: 
    cn.tedu.ssm.mapper: debug  打印sql语句 日志信息 log4j、slf4j，配置到接口的
    

```

### ${num}和#{num}有什么不同？

1）$内容拼接到sql语句，sql注入，字符串类型，整数是不会有注入问题  
mybatis提供$就可以使用，只是使用，字符串类型，有注入风险！不是用户输入的就没有注入风险  
两部分:  
第一部分:```select * from tb_item limit```
第二部分:``````
2）#内部走的?占位符prepareStatement方式，替换？占位符  


### 前端代码

前台请求后台，遇到跨域问题！  
前台8848端口，后台启动8070端口  
浏览器输入请求:ajax(axsio)不能从8848去请求8070，只能请求自己的端口  
![跨域问题](https://i.loli.net/2021/03/02/fmQlFUyzHNBMS3J.png)
怎么解决跨域问题？  




### 搭建前台架构

html页面，创建vue对象，发起ajax（axios），在console.log在结构打印出来  
method定义方法:直接调用  
mounted定义方法:加载完dom树，才调用这个，jQuery.Ready(){}  
调用时机不同  
```html
<!DOCTYPE html>
<html lang="zh-CN">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <script src="../js/vue.js"></script>
</head>

<body>
<div id="app">{{msg}}</div>

<script>
    var vm = new Vue({
        el: '#app',
        data: {
            msg: "hi vue"
        },
        mounted() { //自定义方法就放在mounted中
            axios({ //axios封装ajax，本质ajax
                method: "post", //以post方式请求
                url: "http://localhost:8080/item/list?num=3"
            }).then(//请求之后响应处理方法，返回对象封装response(res)
                //res==response
                res =>{ //数据在res.data属性 返回json字符串
                    console.log(res.data)
                }
            )
        },
        methods: {}
    });
</script>
</body>

</html>
```

### 页面请求的方式:

HTTp协议规定:http://  
GET:URL参数可以看到，提交参数少url最大长度限制:256,512  
POST:url参数看不到，8k=1024*7，安全，提倡  

RESFul,请求形式，和get请求结构很相似（get请求）
http://localhost:8080/item/list?num=10(get请求传参形式)   
RESFul请求形式: http://localhost:8080/item/list/10/标题 安全，数据量比get形成多样，数据量也是少  

除了页面url形式不同，后台controller接收参数地方也要修改  
1）一个参数  
@RequestMapping("/item/list/{num}")  
public List<Item> list(@PathVarable Integer num)  
2）多个参数  
@RequestMapping("/item/lis/{num}/{title})
public List<Item> list(@PathVariable Integer num ,@PathVariable String title)  
3）对象参数  
@RequestMapping("/item/list/{num}/{title}/{sellPoint}")
public List<Item> list(Item item);  

RESTFul参数少，使用它，参数多，使用POST传参  
