package cn.tedu.jtp;

import cn.tedu.jtp.mapper.ItemMapper;
import cn.tedu.jtp.pojo.Item;
import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/*
 * mybatis框架时调用数据库的第三方框架，通常组合springmvc-mybatis
 * spring:如果使用mybatis后期需要在spring框架基础上
 * -mvc架构:model view controller
 * */
//以接口方式完成
public class TestMybatisInterface  {
    //    线程安全 因此全局
    private SqlSessionFactory factory;

    @BeforeEach
    private void  init() throws IOException {
//        2、关联xml
        InputStream is =  Resources.getResourceAsStream("sqlMapConfig.xml");
//       1、创建对象
        factory = new SqlSessionFactoryBuilder().build(is);
    }
    @Test
    public void count(){
        SqlSession session = factory.openSession();
        ItemMapper itemMapper = session.getMapper(ItemMapper.class);//它的参数是一个接口，ItemMapper.java
        Integer count = itemMapper.count();
        System.out.println("记录个数:"+count);
    }
    //    @Test
//    public void mybatis(){
//        SqlSession session = factory.openSession();
////        使用接口方式 不使用 selectlist方法
//        ItemMapper itemMapper = session.getMapper(ItemMapper.class);//它的参数是一个接口，ItemMapper.java
//        List<Item> list = itemMapper.find();
//        for (Item o : list) {
//            System.out.println(o);
//        }
//    }
    @Test
    public void get(){
        SqlSession session = factory.openSession();
//        使用接口方式 不使用 selectlist方法
        ItemMapper itemMapper = session.getMapper(ItemMapper.class);//它的参数是一个接口，ItemMapper.java
        Item item = itemMapper.get(1474391949L);
        System.out.println(item);
    }
    @Test
    public void mybatis(){
        SqlSession session = factory.openSession();
//        使用接口方式 不使用 selectlist方法
        ItemMapper itemMapper = session.getMapper(ItemMapper.class);//它的参数是一个接口，ItemMapper.java
        Item para = new Item();//给pojo对象传递多个值
        para.setTitle("%诺基亚%");
        para.setSellPoint("%超值特价%");
        List<Item> list = itemMapper.find(para);
        for (Item o : list) {
            System.out.println(o);
        }
        //查询不需要事务的，不需要提交，新增，修改，删除，session.commit();
    }
    //    @Test
//    public void insert(){
//        SqlSession session = factory.openSession();
//        ItemMapper itemMapper = session.getMapper(ItemMapper.class);
//        itemMapper.insert();
//        session.commit();//mysql默认为手动提交，若要手动提交 要为true
//    }
    @Test
    public void insert(){
        SqlSession session = factory.openSession();
        ItemMapper itemMapper = session.getMapper(ItemMapper.class);
//        把众多的参数赋值,如果某个字段没有set，包装类型，默认值null
        Item item = new Item();//不是有了框架就不能new
        item.setTitle("苹果11pro");
        item.setSellPoint("好用");
        item.setPrice(10000L);
        item.setNum(99);
        item.setCid(560);
        itemMapper.insert(item);
        session.commit();//mysql默认为手动提交，若要手动提交 要为true
    }
    //    @Test
//    public void update(){
//        SqlSession session = factory.openSession();
//        ItemMapper itemMapper = session.getMapper(ItemMapper.class);
//
//        session.commit();
//    }
    @Test
    public void update(){
        SqlSession session = factory.openSession();
        ItemMapper itemMapper = session.getMapper(ItemMapper.class);
        Item item = new Item();
        item.setId(1474391949L);
//        item.setTitle("华为x40");
        item.setSellPoint("很好");
        item.setPrice(8888L);
        item.setNum(666);
        item.setCid(768);
        item.setStatus(1);
        itemMapper.update(item);
        session.commit();
    }
    //    @Test
//    public void delete(){
//        SqlSession session = factory.openSession();
//        ItemMapper itemMapper = session.getMapper(ItemMapper.class);
//        itemMapper.delete();
//        session.commit();
//    }
    @Test
    public void delete(){
        SqlSession session = factory.openSession();
        ItemMapper itemMapper = session.getMapper(ItemMapper.class);

        itemMapper.delete(1474391946L);
        session.commit();
    }
    @Test
    public void deleteMuch(){
        SqlSession session = factory.openSession();
        ItemMapper mapper = session.getMapper(ItemMapper.class);
        Map<String,Object> map = new HashMap<String, Object>();
        Long [] para = {1L,2L,3L,4L};//集合 Integer[],Long[],String[]
        map.put("ids",para);//只能put一次，key=xml种配置的foreach标签的collection名字
        mapper.deleteMuch(map);
        session.commit();
    }
}
