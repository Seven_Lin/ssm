package cn.tedu.jtp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class JtPApplication {

    public static void main(String[] args) {
        SpringApplication.run(JtPApplication.class, args);
    }

}
