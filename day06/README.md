# day 06

## 知识回顾

### AOP面向切面编程  

j2ee，servlet标准（网络请求request和响应response），tomcat实现了这个servlet标准  
a.servlet的过滤器（filter）,只能用于servlet  
b.springmvc 的拦截器（interceptor），只能用于controller层  
c.spring AOP（aspectJ）,不光b/s还支持c/s，嵌入式，它里面没有servlet/controller，方法  
结论:  
springAOP更加强大，它不局限在request和response的处理  

### springAOP 都有哪些东西？

@Aspect 注解，此类是一个切面类  
@Component  告诉spring我这个类要进行管理，创建对象实例  
@PointCut  切点，谁进行拦截，谁不进行拦截。拦截器中所有的请求都拦截，再判断哪个拦截或哪个不拦截  
判断依据:切点表达式，满足或匹配切点表达式条件true，放行，false，不处理（相当于不拦截）  
方法:修饰符、返回值，包路径、类、方法、参数  
public static void cn.tedu.car.RunApp.main(String[] args){}  
切点表达式  第一个*代表一个通用，..代表多个通用 第二个*代表类名 第三个*代表方法名     
public *cn.tedu.car.service..*.*(..)  

通知:5中，前置通知（pre），后置通知（post），完成返回通知（complete），异常通知，环绕通知（最强大）（around）  
@Around("空方法")  
around方法（ProceedingJoinPoint必须是这个对象，它能获取被拦截对象，也能执行对象方法）  

在所有类上运行，动态给类的方法添加代码  
动态改变别人的类（添加代码），  

### 为什么要用mybatis

数据库结构话，sun jdbc规范，结果集对象ResultSet（表，字段，类型）  
Java是面向对象，model对象，POJO，数据name，String  
数据库技术有自己一套法则，Java语言有自己的一套法则  
1）高并发下JDBC会宕机  
2）对象model（关注点不一样）  
所以需要转换者:hibernate（小型项目）、ibatis、Mybatis（大中型项目）  
hibernate全面向对象ORM框架（HQL），mybatis半面向对象ORM框架（SQL）  
```sql
Select * from tb_item
```

###  怎么用mybatis？ 

1）sqlMapCofig.xml 核心配置文件:JDBC，数据库4个参数  
2）Item.java.POJO代表数据model对象，暂存数据，传递数据    
3) ItemMapper.xml核心独有，发明了一套标签，标识CRUD，查询```<select>sql语句</select> ```     
4）SqlSessionFactory（工厂），线程安全(可以应用到高并发的地方)，全局就一个，术语:单例，成员变量   
5）SqlSession，线程非安全，高并发下会出事，最简单解决办法为每一个用户创建一个（私有）术语:多例（原型），局部变量      
6）.selectList  

### 解决字段和属性不对应  

1）sql自身解决，字段起别名  
![sql的大小写解决方案](https://gitee.com/Seven_Lin/picbed/raw/master/img/20210226103546.png)  

2）利用mybatis，自身特性ResultMap强大  
Result结构，Map映射，新的映射规则  
id id  
title id  
sell_point>ResultMap写一个映射规则
![使用的标签](https://gitee.com/Seven_Lin/picbed/raw/master/img/20210226104753.png)  

### mybatis提供两种方式操作  

1）纯xml方式  
2）xml+接口方式  


### 只创建接口，实现类呢？

![](https://gitee.com/Seven_Lin/picbed/raw/master/img/20210226115520.png)

无需实现类  
真正底层是创建了实现类，动态创建，你看不到，内存中  
动态代理技术动态产生类:  
jdk动态代理($Proxyn)，cglib动态代理（cglib...）  
断电:观察变量的值，要看变量后面设置断点  

### mysql表的主键,int/long支持自增  

auto_increment,数据库会记录当前值，每次新增一条记录会+1
不需要开发者填写  

### sql中值写死了，mybatis动态标签  

配合方法把参数传入，mybatis加入  
单值:id,#{id}  
对象:item，#{id}=item.id,#{title}=item.title,#{sellPoint}=item.sellPoint  

为了实现一句sql语句  
```sql
select * from tb_item where title like "%诺基亚%"
```

![](https://gitee.com/Seven_Lin/picbed/raw/master/img/20210226152538.png) 

## 小结  

### mybatis 完成ORM，对数据封装Java对象  

实现两种方式:  
1）xml方式，selectList，insert，update，delete，selectOne  
2）接口方式，重点  

### 接口方式开发步骤: 

1）写映射xml文件，配置各个标签  
2）在接口中增加它的方法，和标签一一对应，  
```public resultType/resultMap```标签id(parameterType)  
```public List<Item> find(String title)```  
3）session可以按接口方式调用  
```ItemMapper mapper = session.getMapper(ItemMapper.class);```
```mapper.find("%诺基亚%")```

### 映射文件 

1）命名空间(namespace):它要和接口对应，命名空间名字和接口类的名字要一样  
2）每个标签都有返回值，返回值类型:  
a.resultType:int/long/string/Item   
b.resultMap: 对应特有resultMap标签（如果有POJO属性和数据库结果集的column不一致，就要使用它）  
多表联查left join，inner join，right join  
3）参数类型:  
a.parameterType:int/long/string 单个参数，/Item对象参数  
b.parameterMap（废除，被parameterType替代）  

参数和返回值需要时声明，不需要时就不声明  
4）占位符:  
/#{id},代码安全，preparedStatement，调用时内部成为？，防止sql注入，安全    
${id}，代码不安全，直接拼串，没有占位符  
5）自增主键:mysql直接支持，独有auto_increment  
mysql表会记录最后一次插入值，+1.中间删除了一个id，维护成本高，舍弃。  
6）线程安全，和线程非安全  
线程安全对象:支持高并发，加锁，阻塞（等待）  
SqlSessionFactory，成员变量  
非线程安全对象:不支持高并发，不能为成员变量（多个方法共享）  
写在方法内容，局部变量，方法私有内容，每次都要创建  


## 串讲

service层 通常包含接口和实现类，实现类中要使用@service标识  
@component 为通用类  工具类 给spring框架创建实例  
