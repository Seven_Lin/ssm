package cn.tedu.mybatis.pojo;

public class MyBatis {
    private Long id;//编号
    private String userName;//用户名称
    private String cnname; //姓名
    private Integer sex;//性别
    private String mobliePhone;//手机号码

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getCnname() {
        return cnname;
    }

    public void setCnname(String cnname) {
        this.cnname = cnname;
    }

    public Integer getSex() {
        return sex;
    }

    public void setSex(Integer sex) {
        this.sex = sex;
    }

    public String getMobliePhone() {
        return mobliePhone;
    }

    public void setMobliePhone(String mobliePhone) {
        this.mobliePhone = mobliePhone;
    }

    @Override
    public String toString() {
        return "MyBatis{" +
                "id=" + id +
                ", userName='" + userName + '\'' +
                ", cnname='" + cnname + '\'' +
                ", sex=" + sex +
                ", mobliePhone='" + mobliePhone + '\'' +
                '}';
    }
}
