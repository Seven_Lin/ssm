package cn.tedu.cara.aspect;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.stereotype.Component;

@Component //给spring创建实例
@Aspect
public class TimeAspect {
    @Pointcut("execution(public * cn.tedu.cara.service..*(..))")
//    创建一个空方法 pointcut
    private void aopPointCut(){

    }
//    环绕配置
    @Around("aopPointCut()") //value=aopPointCut
//    ProceedingJoinPoint式JoinPoint的子类，ProceedingJoinPoint才有.proceed()返回对象方法
    public Object around (ProceedingJoinPoint JoinPoint) throws Throwable {
        long startTime = System.currentTimeMillis();
//        回调函数
        Object jp = JoinPoint.proceed();
        long endTime = System.currentTimeMillis();
        String cName = JoinPoint.getTarget().getClass().getName();
        System.out.println("类名:"+cName);
//        被拦截方法名
        String mName = JoinPoint.getSignature().getName();
        System.out.println("方法名："+mName);
        System.out.println("执行耗时:"+(endTime-startTime));
        return jp;
    }
}
