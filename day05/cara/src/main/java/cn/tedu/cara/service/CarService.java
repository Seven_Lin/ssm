package cn.tedu.cara.service;

import cn.tedu.cara.pojo.Car;

public interface CarService {
    public Car get();
}
