import cn.tedu.jt.pojo.Item;
import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;

public class TestMybatis2 {
    private SqlSessionFactory factory;
    @BeforeEach
    private  void init() throws IOException { //初始化
        //        2、inputstrinme getResourceAsStream 导包import org.apache.ibatis.io.Resources;
        InputStream is = Resources.getResourceAsStream("sqlMapConfig.xml");
//        1、创建工厂对象
        factory =  new SqlSessionFactoryBuilder().build(is);
    }

    @Test
    public void mybtais()  {

        SqlSession session = factory.openSession();

        List<Item> list = session.selectList("cn.tedu.jt.mapper.ItemMapper.find");
        for (Item o: list) {
            System.out.println(o);
        }
    }
}
