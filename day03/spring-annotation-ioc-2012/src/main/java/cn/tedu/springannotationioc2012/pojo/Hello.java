package cn.tedu.springannotationioc2012.pojo;

import org.springframework.stereotype.Component;

// @Component 这个注解标识，有了这个标识spring才会创建实例
//@Component("hello123") 这个要在TestIoCAnnotation.java中的 ac.getBean("hello123") 才能调用（指定方式）
@Component //约定，类名实例名写法
public class Hello {
    public void hi(){
        System.out.println("spring ioc annotation");
    }
}
