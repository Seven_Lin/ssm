import cn.tedu.springannotationioc2012.pojo.Hello;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class TestIoCAnnotation {
    public static void main(String[] args) {
        ApplicationContext ac = new ClassPathXmlApplicationContext("applicationContext.xml");
        Hello hello = (Hello)ac.getBean("hello");
        hello.hi();

        System.out.println(ac.getBean("user"));
        System.out.println(ac.getBean("dept"));
    }
}
