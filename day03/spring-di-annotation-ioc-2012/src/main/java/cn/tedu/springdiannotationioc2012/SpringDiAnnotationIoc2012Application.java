package cn.tedu.springdiannotationioc2012;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringDiAnnotationIoc2012Application {

    public static void main(String[] args) {
        SpringApplication.run(SpringDiAnnotationIoc2012Application.class, args);
    }

}
