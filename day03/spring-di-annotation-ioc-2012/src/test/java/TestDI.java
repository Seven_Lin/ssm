import cn.tedu.springdiannotationioc2012.pojo.Dept;
import cn.tedu.springdiannotationioc2012.pojo.User;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class TestDI {
    public static void main(String[] args) {
        //        这里注意要遵守驼峰规则
        ApplicationContext ac = new ClassPathXmlApplicationContext("applicationContext.xml");
        User user = (User) ac.getBean("user");
        Dept dept = (Dept) ac.getBean("dept");

////        体现传统关系设定方式
//    user.setDept(dept);//set注入 吧创建dept对象作为set参数传入，这个就体现关联关系

        System.out.println(user);
        System.out.println(dept);
    }
}
