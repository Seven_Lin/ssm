import cn.tedu.praticespringannotationioc.pojo.Student;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class TestIoCXml {
    public static void main(String[] args) {
        ApplicationContext ac = new ClassPathXmlApplicationContext("applicationContext.xml");
        Student stu = (Student) ac.getBean("student");
        System.out.println(stu);
        System.out.println(stu.getAge()+" "+stu.getName());
    }
}
