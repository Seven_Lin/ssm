package cn.tedu.praticespringannotationioc.pojo;

import org.springframework.stereotype.Component;

@Component //这个标识符，
public class Student {
    private String name = "Lin";
    private Integer age = 18 ;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    @Override
    public String toString() {
        return "Student{" +
                "name='" + name + '\'' +
                ", age=" + age +
                '}';
    }
}
