package cn.tedu.springxmlioc;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringXmlIocApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringXmlIocApplication.class, args);
    }

}
