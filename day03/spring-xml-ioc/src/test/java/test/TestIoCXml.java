package test;

import cn.tedu.springxmlioc.pojo.Hello;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class TestIoCXml {
    public static void main(String[] args) {
//        创建spring环境，死语法
       ApplicationContext ac = new ClassPathXmlApplicationContext("applictionContext.xml");
//        获取Hello Bean 对象，调用其方法,底层spring创建对象
        Hello hello = (Hello) ac.getBean("hello"); //<bean>的id属性

//        直接调用对象方法
        System.out.println(hello);
        hello.hi();

//        不会创建对象实例 会报错 报错No bean named 'user' available
        System.out.println(ac.getBean("user"));
        System.out.println(ac.getBean("dept"));
    }
}
