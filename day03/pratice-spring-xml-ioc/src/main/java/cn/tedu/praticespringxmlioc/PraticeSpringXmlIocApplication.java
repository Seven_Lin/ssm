package cn.tedu.praticespringxmlioc;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PraticeSpringXmlIocApplication {

    public static void main(String[] args) {
        SpringApplication.run(PraticeSpringXmlIocApplication.class, args);
    }

}
