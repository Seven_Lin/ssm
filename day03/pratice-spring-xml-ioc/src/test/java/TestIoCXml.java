import cn.tedu.praticespringxmlioc.pojo.Hello;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class TestIoCXml {
    public static void main(String[] args) {
//        创建容器环境，死语法
        ApplicationContext ac = new ClassPathXmlApplicationContext("applicationContext.xml");
//        这里填写的时hBean的id
        Hello hello = (Hello) ac.getBean("hello");
        hello.hi();
//        cn.tedu.praticespringxmlioc.pojo.Hello@52f759d7
        System.out.println(hello);
    }
}
