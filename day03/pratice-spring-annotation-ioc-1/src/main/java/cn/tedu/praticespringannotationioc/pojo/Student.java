package cn.tedu.praticespringannotationioc.pojo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class Student {
    private String name = "Lin";
    private Integer age = 19;
    @Autowired //自动执行
//    使用类名为School的schName属性
    private School schName;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public School getSchName() {
        return schName;
    }

    public void setSchName(School schName) {
        this.schName = schName;
    }

    @Override
    public String toString() {
        return "Student{" +
                "name='" + name + '\'' +
                ", age=" + age +
                ", schName=" + schName +
                '}';
    }
}
