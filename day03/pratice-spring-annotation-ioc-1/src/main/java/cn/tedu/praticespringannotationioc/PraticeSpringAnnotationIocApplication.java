package cn.tedu.praticespringannotationioc;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PraticeSpringAnnotationIocApplication {

    public static void main(String[] args) {
        SpringApplication.run(PraticeSpringAnnotationIocApplication.class, args);
    }

}
