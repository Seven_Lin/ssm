package cn.tedu.praticespringannotationioc.pojo;

import org.springframework.stereotype.Component;

@Component
public class School {
    private String schName ="城建";

    public String getSchName() {
        return schName;
    }

    public void setSchName(String schName) {
        this.schName = schName;
    }

    @Override
    public String toString() {
        return "School{" +
                "schName='" + schName + '\'' +
                '}';
    }
}
