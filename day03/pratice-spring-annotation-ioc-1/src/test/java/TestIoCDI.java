import cn.tedu.praticespringannotationioc.pojo.School;
import cn.tedu.praticespringannotationioc.pojo.Student;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class TestIoCDI {
    public static void main(String[] args) {
        ApplicationContext ac = new ClassPathXmlApplicationContext("applicationContext.xml");
        Student student = (Student) ac.getBean("student");
        School school = (School) ac.getBean("school");
        System.out.println(student);
        System.out.println(school);
    }
}
